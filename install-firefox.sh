#!/bin/bash
killall firefox firefox-bin &> /dev/null
URL1="https://download.mozilla.org/?product=firefox"
[ -z "$CHANNEL" ] && URL2="${URL1}${CHANNEL}" || URL2="${URL1}-${CHANNEL}"
URL="${URL2}-latest-ssl&os=linux64&lang="
wget -c -O firefox.tar.bz2 "${URL}${LANG:0:2}"
tar xvjf firefox.tar.bz2
sudo cp -r firefox /opt/
sudo chown -R $USER:$USER /opt/firefox
sudo cp firefox.desktop /usr/share/applications
rm -r firefox
rm firefox.tar*
/opt/firefox/firefox &!
